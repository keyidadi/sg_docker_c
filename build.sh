#!/bin/bash

REGISTRY=registry.cn-shanghai.aliyuncs.com/smartfactory001
DATE=$(date "+%Y%m%d")

rm -rf src/*.class src/*.exe files/*
mkdir -p files/usr/bin files/usr/share/java

cd src
javac -cp org.eclipse.paho.client.mqttv3-1.2.2.jar PLCDemo.java
javac -cp mongo-java-driver-3.11.1.jar MongoDBExample.java
javac -cp RXTXcomm.jar SerialPortTest.java
javac GPIOPortTest.java

csc SerialPortTest.cs
csc GPIOPortTest.cs

mv *.class ../files/usr/share/java
mv *.exe ../files/usr/bin
cp start.sh ../files/usr/bin

cd ..
docker build -t ${REGISTRY}/docker_c_test:${DATE} .
docker push ${REGISTRY}/docker_c_test:${DATE}

#rm ../files/*
