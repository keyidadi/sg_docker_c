FROM registry.cn-shanghai.aliyuncs.com/smartfactory001/docker_c_base:20200523
RUN [ "cross-build-start" ]

# deploy files
COPY files /

ENV CLASSPATH=/usr/share/java

RUN [ "cross-build-end" ]
CMD [ "/usr/bin/start.sh" ]
