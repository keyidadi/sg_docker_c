import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class SerialPortTest
{
    public static void main(String [] args)
    {
        String port = "/dev/COM1";

        SerialPortTest myTest = new SerialPortTest();
        myTest.Test(port);
    }

    // Constructor
    public SerialPortTest()
    {
    }

    public void Test(String port)
    {
        // SD-123
        // byte[] tx = new byte[] {0x01, 0x03, 0x02, 0x00, 0x00, 0x03, 0x04, 0x73};
        // zz sensor
        byte [] tx = new byte[] {0x06, 0x03, 0x00, 0x00, 0x00, 0x02, (byte)0xC5, (byte)0xBC};
        byte [] data = new byte[10];

        CommPort commPort;
        CommPortIdentifier portIdentifier;
        SerialPort serialPort;

        try {
            portIdentifier = CommPortIdentifier.getPortIdentifier(port);
        }
        catch (NoSuchPortException e) {
            System.out.println("No such serial port: " + port);
            return;
        }
        try {
            commPort = portIdentifier.open(port, 5000);
        }
        catch (PortInUseException e) {
            System.out.println("Serial port in use: " + port);
            return;
        }
        
        try {
            if (commPort instanceof SerialPort) {
                serialPort = (SerialPort) commPort;
                
                serialPort.setSerialPortParams(9600, 8, 1, 0);

            } else {
                System.out.println("Cannot open serial port " + port);
                return;
            }

            OutputStream out_stream = new BufferedOutputStream(serialPort.getOutputStream());
            InputStream in_stream = new BufferedInputStream(serialPort.getInputStream());

            while(true)
            {
                out_stream.write(tx);
                out_stream.flush();

                System.out.println("Send Data: " + bytesToHexString(tx));

                serialPort.enableReceiveTimeout(5000);

                int count = 0;

                // SD-123
                // int len = 10;

                // zz sensor
                int len = 9;
                while (count < len) {
                    count += in_stream.read(data, count, len - count);
                }
                System.out.println("Receive Data: " + bytesToHexString(data));

                Thread.sleep(1000);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            commPort.close();
        }
    }

    public final String bytesToHexString(byte[] bArray) {
        StringBuffer sb = new StringBuffer(bArray.length);
        String sTemp;
        for (int i = 0; i < bArray.length; i++) {
        sTemp = Integer.toHexString(0xFF & bArray[i]);
        if (sTemp.length() < 2)
        sb.append(0);
        sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }
}
