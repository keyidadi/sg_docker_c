import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class MongoDBExample{
    public static void main( String args[] ){
        try{
            // 连接到 mongodb 服务
            MongoClient mongoClient = new MongoClient("172.17.0.1" , 27017);

            // 连接到数据库
            MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
            System.out.println("Connect to database successfully");
            try{
                mongoDatabase.createCollection("students");
                System.out.println("Collection create successfully");
            }catch(Exception e){
                System.out.println("Collection exists");
            }

            MongoCollection<Document> collection = mongoDatabase.getCollection("students");
            System.out.println("Collection students select successfully");

            //插入文档
            /**
            * 1. 创建文档 org.bson.Document 参数为key-value的格式
            * 2. 创建文档集合List<Document>
            * 3. 将文档集合插入数据库集合中 mongoCollection.insertMany(List<Document>) 插入单个文档可以用 mongoCollection.insertOne(Document)
            * */
            List<Document> documents = new ArrayList<Document>();

            Document document1 = new Document("id", "20190101").
            append("name", "Alpha").
            append("age", 20).
            append("gender", "male");

            Document document2 = new Document("id", "20190102").
            append("name", "Beta").
            append("age", 25).
            append("gender", "female");

            documents = new ArrayList<Document>();
            documents.add(document1);
            documents.add(document2);
            collection.insertMany(documents);
            System.out.println("Documents insert successfully:");

            FindIterable<Document> findIterable = collection.find();
            MongoCursor<Document> mongoCursor = findIterable.iterator();
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }

            //检索所有文档
            /**
            * 1. 获取迭代器FindIterable<Document>
            * 2. 获取游标MongoCursor<Document>
            * 3. 通过游标遍历检索出的文档集合
            * */
            findIterable = collection.find(Filters.lt("age", 25));
            mongoCursor = findIterable.iterator();
            System.out.println("Documents search successfully:");
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }

            //更新文档   将文档中name=Alpha的文档修改为gender=female
            collection.updateMany(Filters.eq("name", "Alpha"), new Document("$set",new Document("gender", "female")));
            System.out.println("Documents update successfully:");
            //检索查看结果
            findIterable = collection.find();
            mongoCursor = findIterable.iterator();
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }

            //删除符合条件的第一个文档
            collection.deleteOne(Filters.eq("name", "Beta"));
            //删除所有符合条件的文档
            collection.deleteMany(Filters.gt("age", 24));
            System.out.println("Documents delete successfully:");
            //检索查看结果
            findIterable = collection.find();
            mongoCursor = findIterable.iterator();
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
}