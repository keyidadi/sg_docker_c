using System;
using System.IO;
using System.Text;
using System.Threading;

public class GPIOPortTest
{
    string gpio_path = "/sys/class/gpio/gpio{0}/value";

    public static void Main(string[] args)
    {
        int [] gpio_in = new int [] {4, 5, 6};
        int [] gpio_out = new int [] {12, 13, 16};
        
        GPIOPortTest myTest = new GPIOPortTest();
        myTest.Test(gpio_in, gpio_out);
    }

    public void Test(int [] gpio_in, int [] gpio_out)
    {
        FileStream in_stream = null, out_stream = null;
        byte [] data = new byte[1];
        char [] chars = null;
        string gpio = "";

        try{
            foreach (int i in gpio_in)
            {
                gpio = string.Format(gpio_path, i);
                in_stream= File.Open(gpio, FileMode.Open, FileAccess.Read);
                in_stream.Read(data, 0, 1);
                int count = Encoding.Default.GetCharCount(data, 0, 1);
                chars = new char[count];
                Encoding.Default.GetChars(data, 0, 1, chars, 0);

                Console.WriteLine("Read gpio" + i + " : " + new string(chars));

                in_stream.Close();
                Thread.Sleep(1000);
            }

            foreach (int i in gpio_out)
            {
                gpio = string.Format(gpio_path, i);
                out_stream = File.Open(gpio, FileMode.Open, FileAccess.Write);

                data = Encoding.Default.GetBytes("1");
                out_stream.Write(data, 0, data.Length);
                out_stream.Flush();
                Console.WriteLine("Write gpio" + i + " : 1");

                Thread.Sleep(1000);

                data = Encoding.Default.GetBytes("0");
                out_stream.Write(data, 0, data.Length);
                out_stream.Flush();
                Console.WriteLine("Write gpio" + i + " : 0");
                out_stream.Close();

                Thread.Sleep(1000);
            }
        }
        finally
        {
            if (in_stream != null)
                in_stream.Close();
            if (out_stream != null)
                out_stream.Close();
        }
    }
}
