using System;
using System.Threading;
using System.IO.Ports;

public class SerialPortTest
{
    public static void Main(string[] args)
    {
        string port = "/dev/COM1";

        if (args.Length > 0)
        {
            port = args[0];
        }

        SerialPortTest myTest = new SerialPortTest();
        myTest.Test(port);
    }

    private SerialPort mySerial;

    // Constructor
    public SerialPortTest()
    {
    }

    public void Test(string port)
    {
        // SD-123
        byte[] tx = new byte[] {0x01, 0x03, 0x02, 0x00, 0x00, 0x03, 0x04, 0x73};
        // zz sensor
        // byte[] tx = new byte[] {0x06, 0x03, 0x00, 0x00, 0x00, 0x02, 0xC5, 0xBC};

        if (mySerial != null)
            if (mySerial.IsOpen)
                mySerial.Close();

        Console.WriteLine("Open port '" + port + "' at 9600, 8, N, 1");

        mySerial = new SerialPort(port, 9600, Parity.None, 8, StopBits.One);
        mySerial.ReadTimeout = 500;
        mySerial.WriteTimeout = 500;
        mySerial.Handshake = Handshake.None;
        mySerial.Open();
        try{
            while(true)
            {
                mySerial.Write(tx, 0, 8);
                Console.WriteLine("Send Data: " + BitConverter.ToString(tx));

                // Should output some information about your modem firmware
                // SD-123
                Console.WriteLine("Receive Data: " + ReadBytes(10));

                // zz sensor
                // Console.WriteLine("Receive Data: " + ReadBytes(9));
                Thread.Sleep(1000);
            }
        }
        finally
        {
            Console.WriteLine("Close port '" + port + "'");
            mySerial.Close();
        }
    }

    public string ReadBytes(int len)
    {
        byte tmpByte;
        string rxString = "";

        tmpByte = (byte) mySerial.ReadByte();

        for (int i = 0; i < len; i++) {
            rxString += BitConverter.ToString(new byte[] {tmpByte});
            tmpByte = (byte) mySerial.ReadByte();
        }

        return rxString;
    }
}
