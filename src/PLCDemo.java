import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class PLCDemo {

	public static void main(String[] args) {

        String publish_topic = "SG/PLCD/PLC_WRITE";
        String subscribe_topic = "SG/PLCD/PLC_WRITE_RESULT";
        String content = "hello world";
        int qos = 1;
        String broker = "tcp://172.17.0.1:1883";
        //String broker = "tcp://192.168.1.2:1883";
        String clientId = "PLC_Demo_Java_Client";
        // 内存存储
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            // 创建客户端
            MqttClient client = new MqttClient(broker, clientId, persistence);
            // 创建链接参数
            MqttConnectOptions options = new MqttConnectOptions();
            // 在重新启动和重新连接时记住状态
            options.setCleanSession(true);
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            // 设置回调函数
            client.setCallback(new MqttCallback() {

                public void connectionLost(Throwable cause) {
                    System.out.println("Connection lost");
                }

                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    System.out.println("New message arrived:");
                    System.out.println("Topic: " + topic);
                    System.out.println("QoS: " + message.getQos());
                    System.out.println("Message content: " + new String(message.getPayload()));
                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                    System.out.println("deliveryComplete---------" + token.isComplete());
                }
            });
            
            // 建立连接
            client.connect(options);
            System.out.println("Broker conencted");
            //订阅消息
            client.subscribe(subscribe_topic, qos);
            System.out.println("Subscribe topic: " + subscribe_topic);

            client.subscribe("SG/PLCD/PLC_CONNECT_RESULT", qos);
            System.out.println("Subscribe topic: " + "SG/PLCD/PLC_CONNECT_RESULT");

            MqttMessage message = new MqttMessage("{\"ipaddr\":\"192.168.1.10\", \"port\":0, \"model\":\"Siemens\", \"config\":\"SiemensPLCS:S200Smart\"}".getBytes());
            // 设置消息的服务质量
            message.setQos(qos);
            // 发布消息
            client.publish("SG/PLCD/PLC_CONNECT", message);

            Thread.sleep(2000);

            message = new MqttMessage("{\"ipaddr\":\"192.168.1.10\",\"addr\":\"Q0.7\",\"type\":\"Boolean\",\"len\":1,\"value\":\"AQ==\",\"seq\":10001}".getBytes());
            // 设置消息的服务质量
            message.setQos(qos);
            // 发布消息
            client.publish(publish_topic, message);

            Thread.sleep(1000);

            message = new MqttMessage("{\"ipaddr\":\"192.168.1.10\",\"addr\":\"Q0.7\",\"type\":\"Boolean\",\"len\":1,\"value\":\"AA==\",\"seq\":10002}".getBytes());
            // 设置消息的服务质量
            message.setQos(qos);
            // 发布消息
            client.publish(publish_topic, message);

            Thread.sleep(1000);

            // 断开连接
            client.disconnect();
            // 关闭客户端
            client.close();

            System.out.println("MQTT消息发送成功");
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
        catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
	}
}
