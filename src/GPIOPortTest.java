import java.io.*;
import java.lang.*;

public class GPIOPortTest
{
    String gpio_path = "/sys/class/gpio/gpio%d/value";

    public static void main(String[] args)
    {
        int [] gpio_in = new int [] {4, 5, 6};
        int [] gpio_out = new int [] {12, 13, 16};
        
        GPIOPortTest myTest = new GPIOPortTest();
        myTest.Test(gpio_in, gpio_out);
    }

    public void Test(int [] gpio_in, int [] gpio_out)
    {
        FileReader reader;
        FileWriter writer;
        BufferedReader in_buf = null;
        BufferedWriter out_buf = null;
        String line;

        try
        {
            for (int i:gpio_in)
            {
                reader = new FileReader(String.format(gpio_path, i));
                in_buf = new BufferedReader(reader);
                line = in_buf.readLine();
                in_buf.close();
                System.out.println("Read gpio " + i + " : " + line);

                Thread.sleep(1000);
            }

            for (int i:gpio_out)
            {
                writer = new FileWriter(String.format(gpio_path, i));
                out_buf = new BufferedWriter(writer);

                out_buf.write("1");
                out_buf.flush();
                System.out.println("Write gpio " + i + " : 1");

                Thread.sleep(1000);

                out_buf.write("0");
                out_buf.flush();
                System.out.println("Write gpio " + i + " : 0");
                out_buf.close();

                Thread.sleep(1000);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
